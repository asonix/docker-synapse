ARG REPO_ARCH

FROM ${REPO_ARCH}/alpine:edge

ENV UID=991
ENV GID=991

RUN \
  mkdir /opt/synapse && \
  addgroup --gid "$GID" synapse && \
  adduser \
  --disabled-password \
  --gecos "" \
  --home /opt/synapse \
  --ingroup synapse \
  --no-create-home \
  --uid "$UID" \
  synapse && \
  chown -R synapse:synapse /opt/synapse && \
  apk add synapse tini

EXPOSE 8008/tcp

VOLUME /media_store

USER synapse
WORKDIR /opt/synapse

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/usr/bin/synctl", "--no-daemonize", "start", "/etc/synapse/homeserver.yaml"]
